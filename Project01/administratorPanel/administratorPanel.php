<!DOCTYPE html>
<html>
<head>
	 <meta charset=UTF-8>
	    <title>Brainster</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="styleAdministrator.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
     <div class="container-fluid">
     	<div class="row">
     		<div class="col-md-4">
     			<form method="POST" action="administratorDatabase.php">
     				<label for="inputUrl">Лого</label><br>
     				<input type="text" name="inputUrl"><br>
     				<label for="mainTitle">Наслов</label><br>
     				<input type="text" name="mainTitle"><br>
     				<label for="title">Поднаслов</label><br>
     				<input type="text" name="title"><br>
     				<label>Опис</label><br>
     				<textarea name="opis" rows="10" cols="50"></textarea><br>
     				<input type="submit" name="submit" value="Испрати">
     			</form>
     		</div>
     	</div>
     </div>
</body>
</html>