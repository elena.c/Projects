<?php 
    
    $email = $_POST['email'];
    $telephone = $_POST['tel'];
    $name = $_POST['text'];

    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    	
    

    $dbhost = "localhost:3306";
    $dbname = "project_messages";
    $dbuser = "elena";
    $dbpass = "elena";

	try {


	    $pdo = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass);
		
    	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $pdo->prepare("INSERT INTO messages(email,telephone,company_name) VALUES (?,?,?)");
	    $stmt->bindParam(1,$email);
	    $stmt->bindParam(2,$telephone);
	    $stmt->bindParam(3,$name);
	    $stmt->execute();
	    $pdo = null;
	    header('location:../project.php');
	    die();
	} catch (PDOException $e) {
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}

}else{
	header('location:form.php');
	die();
}
	
