<!DOCTYPE html>
<html>
<head>
	    <meta charset=UTF-8>
	    <title>Brainster</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="styleForm.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
     <div class="container-fluid">
     	<div class="row header">
     		<div class="col-md-2 col-md-offset-1 logo">
     			<img src="Brainster-Logo-03.png">
     		</div>
     		<div class="col-md-1 col-md-offset-7">
     			<a href="/firstProjectcode/project.php"><p  class="goBack">Почетна</p></a>
     		</div>
     	</div>
     </div>
     <div class="container-fluid back">
     	<div class="row blackBack">
     		<div class="col-md-3 col-md-offset-4 form1">
     			<h2 class="text-center">Вработи наши студенти</h2>
     			<form method="POST" action="databaseConnect.php">
     				<label for="email">Емаил</label><br>
     				<input type="email" name="email" id="email" class="inp" required="required"><br>
     				<label for="tel">Телефонски број</label><br>
     				<input type="tel" name="tel" id="tel" class="inp" required="required"><br>
     				<label for="text">Име на фирма</label><br>
     				<input type="text" name="text" id="text" class="inp" required="required"><br>
     				<button type="submit" class="submitBtn">Испрати</button>
     			</form>
     		</div>
     	</div>
     </div>
     <div class="container-fluid">
	<div class="row footer">
		<div class="col-md-3 col-md-offset-5">
			<p>Made with <i class="fas fa-heart"></i> by<img src="Brainster-Logo-03.png">     Say Hi! - Term </p>
		</div>
	</div>
</div>
</body>
</html>