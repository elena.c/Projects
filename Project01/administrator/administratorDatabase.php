<?php 

$img = $_POST['inputUrl'];
$maintitle = $_POST['mainTitle'];
$title = $_POST['title'];
$descrp = $_POST['tekst'];

$dbhost = "localhost:3306";
$dbname = "project_messages";
$dbuser = "elena";
$dbpass = "elena";

try {
	$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $pdo->prepare("INSERT INTO karticki(slika,naslov,podnaslov,opis) VALUES (?,?,?,?)");
	    $stmt->bindParam(1,$img);
	    $stmt->bindParam(2,$maintitle);
	    $stmt->bindParam(3,$title);
	    $stmt->bindParam(4,$descrp);
	    $stmt->execute();
	    $pdo = null;
	    header('location:redirect.php');
} catch (PDOException $e) {
	print "Error!" . $e->getMessage() . "<br>";
	die();
}