<!DOCTYPE html>
<html>
<head>
	<meta charset=UTF-8>
	    <title>Brainster</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
     <div class="container-fluid">
     	<div class="row">
     		<div class="col-md-4 col-md-offset-4 text-center">
     			<img src="../img/Brainster-Logo-03.png" style="height: 200px;margin-top: 25%;">
     			<h1>Успешно внесовте нов проект!</h1>
     		</div>
     	</div>
     </div>
</body>
</html>