<!DOCTYPE html>
<html>
<head>
	 <meta charset=UTF-8>
	    <title>Brainster</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="styleAdministrator.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
   <div class="container-fluid back">
   	<div class="row blackBack">
   		<div class="col-md-4 formSignIn">
   			<div class="img">
   		    	<img src="Brainster-Logo-03.png">
   	    	</div>
   			<form method="POST" action="adminLoggedIn.php">
   				<label for="email">Емаил</label><br>
   				<input type="email" name="email" class="inp"><br>
   				<label for="password">Лозинка</label><br>
   				<input type="password" name="password" class="inp"><br>
   				<input type="submit" name="submit" class="submitBtn" value="Најави се">
   			</form>
   		</div>
   	</div>
   </div>
</body>
</html>