-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2018 at 06:28 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_messages`
--

-- --------------------------------------------------------

--
-- Table structure for table `karticki`
--

CREATE TABLE `karticki` (
  `id` int(11) NOT NULL,
  `slika` varchar(255) DEFAULT NULL,
  `naslov` varchar(20) DEFAULT NULL,
  `podnaslov` varchar(20) DEFAULT NULL,
  `opis` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `karticki`
--

INSERT INTO `karticki` (`id`, `slika`, `naslov`, `podnaslov`, `opis`) VALUES
(1, 'http://pogon.co/wp-content/uploads/2015/12/Brainster-Logo.png', 'Brainster', 'Brainster Codepreneu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at purus tincidunt, elementum n'),
(2, 'http://pogon.co/wp-content/uploads/2015/12/Brainster-Logo.png', 'Brainster', 'Brainster Codepreneu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at purus tincidunt, elementum n'),
(3, 'http://pogon.co/wp-content/uploads/2015/12/Brainster-Logo.png', 'Brainster', 'Brainster Codepreneu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at purus tincidunt, elementum n'),
(4, 'http://pogon.co/wp-content/uploads/2015/12/Brainster-Logo.png', 'Brainster', 'Brainster Codepreneu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at purus tincidunt, elementum n'),
(5, 'http://pogon.co/wp-content/uploads/2015/12/Brainster-Logo.png', 'Brainster', 'Brainster Codepreneu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at purus tincidunt, elementum n'),
(8, 'http://pogon.co/wp-content/uploads/2015/12/Brainster-Logo.png', 'Brainster', 'Brainster Codepreneu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at purus tincidunt, elementum n');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `company_name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karticki`
--
ALTER TABLE `karticki`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `karticki`
--
ALTER TABLE `karticki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
