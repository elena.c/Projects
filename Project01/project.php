<?php 
     include "getFromDatabase.php";
 ?>

<!DOCTYPE html>
<html>
<head>
	    <meta charset=UTF-8>
	    <title>Brainster</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> 
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <nav class="navbar-default navBarColor">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand logo" href="#"><img src="img/Brainster-Logo-03.png"></a>
    </div>
    <div class="Btn1">
  	<button class="btnMobile navbar-toggler pull-right toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1"
		        aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i class="fas fa-bars"></i></span>
	</button>
    </div>
        <div class="list-navbar collapse navbar-collapse" id="navbarSupportedContent1">
		    <ul class="nav navbar-nav mr-auto pull-right marginRight navfontColor">
		      <li class="mobileFirstLink nav-item"><a  href="http://codepreneurs.co/">Академија за <br>Програмирање</a></li>
		      <li class="nav-item"><a href="https://brainster.co/">Академија за <br>Маркетинг</a></li>
		      <li class="nav-item"><a href="https://blog.brainster.co/">Блог</a></li>
		      <li class="nav-item"><a href="/firstProjectcode/form/form.php">Вработи наши<br> студенти</a></li>
		    </ul>
    </div>
  </div>
</nav>
<div class="container-fluid backImg">
	<div class="row back2">
	<div class="text-center textBackground">
	<h1>Brainster.xyz Labs</h1>
	<h3>Проекти на студентите на академиите за програмирање<br>
и маркетинг на Brainster</h3>
</div>
</div>
</div>
<div class="container-fluid text-center">
	<div class="row">
		<?php 
        foreach ($results as $row) { ?>	 
		<div class="col-md-4 Card">
			<img src="<?php echo $row['slika']; ?>">
			<h4><?php echo $row['naslov'];?></h4>
			<h5><?php echo $row['podnaslov']; ?></h5>
			<p><?php echo $row['opis']; ?></p>
		</div>
		<?php  } ?>
	</div>

</div>
<div class="container-fluid">
	<div class="row footer">
		<div class="col-md-3 col-md-offset-5">
			<p>Made with <i class="fas fa-heart"></i> by<img src="img/Brainster-Logo-03.png">    <a href="https://www.facebook.com/codepreneurs/" target="_blank"> Say Hi!</a> - Term </p>
		</div>
	</div>
</div>


</body>
</html>